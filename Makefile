.PHONY : all clean help  jxlife jxlife-init jxlife-build jxlife-link
#-- build all
all:  jxlife jxlife-init jxlife-build jxlife-link
#-- build jxlife
jxlife: jxlife-init jxlife-build jxlife-link

# ==== jxlife

#-- jxlife: initialize for debug build
jxlife-init:
	@mkdir -p debug
	@mkdir -p debug/jxlife-dir/src

-include debug/jxlife-dir/src/Animal.cpp.d
debug/jxlife-dir/src/Animal.cpp.o: src/Animal.cpp
	c++ -MMD -c "-Isrc" "-I/usr/include"  -std=c++20 -D_GNU_SOURCE  src/Animal.cpp -o debug/jxlife-dir/src/Animal.cpp.o
-include debug/jxlife-dir/src/App.cpp.d
debug/jxlife-dir/src/App.cpp.o: src/App.cpp
	c++ -MMD -c "-Isrc" "-I/usr/include"  -std=c++20 -D_GNU_SOURCE  src/App.cpp -o debug/jxlife-dir/src/App.cpp.o
-include debug/jxlife-dir/src/Cell.cpp.d
debug/jxlife-dir/src/Cell.cpp.o: src/Cell.cpp
	c++ -MMD -c "-Isrc" "-I/usr/include"  -std=c++20 -D_GNU_SOURCE  src/Cell.cpp -o debug/jxlife-dir/src/Cell.cpp.o
-include debug/jxlife-dir/src/EnvironmentInfo.cpp.d
debug/jxlife-dir/src/EnvironmentInfo.cpp.o: src/EnvironmentInfo.cpp
	c++ -MMD -c "-Isrc" "-I/usr/include"  -std=c++20 -D_GNU_SOURCE  src/EnvironmentInfo.cpp -o debug/jxlife-dir/src/EnvironmentInfo.cpp.o
-include debug/jxlife-dir/src/Grid.cpp.d
debug/jxlife-dir/src/Grid.cpp.o: src/Grid.cpp
	c++ -MMD -c "-Isrc" "-I/usr/include"  -std=c++20 -D_GNU_SOURCE  src/Grid.cpp -o debug/jxlife-dir/src/Grid.cpp.o
-include debug/jxlife-dir/src/IntParam.cpp.d
debug/jxlife-dir/src/IntParam.cpp.o: src/IntParam.cpp
	c++ -MMD -c "-Isrc" "-I/usr/include"  -std=c++20 -D_GNU_SOURCE  src/IntParam.cpp -o debug/jxlife-dir/src/IntParam.cpp.o
-include debug/jxlife-dir/src/main.cpp.d
debug/jxlife-dir/src/main.cpp.o: src/main.cpp
	c++ -MMD -c "-Isrc" "-I/usr/include"  -std=c++20 -D_GNU_SOURCE  src/main.cpp -o debug/jxlife-dir/src/main.cpp.o

#-- jxlife: build source files
jxlife-build: debug/jxlife-dir/src/Animal.cpp.o debug/jxlife-dir/src/App.cpp.o debug/jxlife-dir/src/Cell.cpp.o debug/jxlife-dir/src/EnvironmentInfo.cpp.o debug/jxlife-dir/src/Grid.cpp.o debug/jxlife-dir/src/IntParam.cpp.o debug/jxlife-dir/src/main.cpp.o 

debug/jxlife: debug/jxlife-dir/src/Animal.cpp.o debug/jxlife-dir/src/App.cpp.o debug/jxlife-dir/src/Cell.cpp.o debug/jxlife-dir/src/EnvironmentInfo.cpp.o debug/jxlife-dir/src/Grid.cpp.o debug/jxlife-dir/src/IntParam.cpp.o debug/jxlife-dir/src/main.cpp.o 
	c++ debug/jxlife-dir/src/Animal.cpp.o debug/jxlife-dir/src/App.cpp.o debug/jxlife-dir/src/Cell.cpp.o debug/jxlife-dir/src/EnvironmentInfo.cpp.o debug/jxlife-dir/src/Grid.cpp.o debug/jxlife-dir/src/IntParam.cpp.o debug/jxlife-dir/src/main.cpp.o    -lpthread -lglut -lGLU -lGL  -o debug/jxlife

#-- jxlife: link
jxlife-link: debug/jxlife

# used to pass args in cpp and gtest cmd lines
%:
	@:
#-- jxlife: run executable
jxlife-run: jxlife-link
	@debug/jxlife $(filter-out $@,$(MAKECMDGOALS))

#-- jxlife: clean files in this target
jxlife-clean:
	rm -f debug/jxlife-dir/src/*.o
	rm -f debug/jxlife-dir/src/*.d
	rm -f debug/jxlife

#-- clean files
clean: jxlife-clean 

help:
	@printf "Available targets:\n"
	@printf "  [32;01mall                                [0m build all\n"
	@printf "  [32;01mclean                              [0m clean files\n"
	@printf "  [32;01mhelp                               [0m this help info\n"
	@printf "  [32;01mjxlife                             [0m build jxlife\n"
	@printf "    [32;01mjxlife-build                       [0m jxlife: build source files\n"
	@printf "    [32;01mjxlife-clean                       [0m jxlife: clean files in this target\n"
	@printf "    [32;01mjxlife-init                        [0m jxlife: initialize for debug build\n"
	@printf "    [32;01mjxlife-link                        [0m jxlife: link\n"
	@printf "    [32;01mjxlife-run                         [0m jxlife: run executable\n"
	@printf "\n"
