#pragma once

#include <ostream>

// --------------------
class IntParam {
public:
    // --------------------
    IntParam(int defaultval, int min, int max)
    {
        mValue = defaultval;
        mMin = min;
        mMax = max;
    }

    // --------------------
    operator int() const
    {
        return mValue;
    }

    // --------------------
    bool operator>(const IntParam& other) const
    {
        return mValue > other.mValue;
    }

    // --------------------
    bool operator>(int other) const
    {
        return mValue > other;
    }

    // --------------------
    int operator--()
    {
        if (mValue > mMin) {
            mValue--;
        }
        return mValue;
    }

    // --------------------
    int operator++()
    {
        if (mValue < mMax) {
            mValue++;
        }
        return mValue;
    }

    // --------------------
    int operator=(int v)
    {
        mValue = v;
        return mValue;
    }

    friend std::ostream& operator<<(std::ostream& os, const IntParam& p);

protected:
    int mValue;
    int mMin;
    int mMax;
};

extern std::ostream& operator<<(std::ostream& os, const IntParam& p);
