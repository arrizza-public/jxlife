#pragma once

#include "Animal.h"
#include "jColor.h"
#include "jRect.h"

// --------------------
class Cell {
public:
    Cell();
    void update_colors();
    void init_animal();
    void update_animal(int row, int col);
    void update_plant_life();
    void set_sunshine();
    Animal mAnimal;
    jColor get_color();
    // TODO delete?
    // void set_color(const jColor &c);
    // void inc_color(int inc);

    jRect rect {};

private:
    void move_closer_to_food(int row, int col);
    static bool is_better_food(int r, int c, int& bestgreen);
    void create_new_animal(int row, int col);
    static bool is_better_parent(int r, int c, int& bestenergy);
    void move_closer_to_friend(int row, int col);
    static bool is_better_friend(int friendr, int friendc, int& bestenergy);

    // a cell can contain sunshine, plantlife, dirt and an animal (all optional)
    jColor color;
    bool m_has_sunshine;
    int m_plant_life;
    int m_dirt;
};
