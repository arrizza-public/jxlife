#include "Animal.h"
#include "EnvironmentInfo.h"
#include "jColor.h"

#include <cstdlib>

static jColor WHITE(255, 255, 255);

static const jColor ANIMAL_9(0, 0, 255);
static const jColor ANIMAL_8(0, 0, 245);
static const jColor ANIMAL_7(0, 0, 235);
static const jColor ANIMAL_6(0, 0, 225);
static const jColor ANIMAL_5(0, 0, 215);
static const jColor ANIMAL_4(0, 0, 205);
static const jColor ANIMAL_3(0, 0, 195);
static const jColor ANIMAL_2(0, 0, 185);
static const jColor ANIMAL_1(0, 0, 175);
static const jColor ANIMAL_0(0, 0, 165);
static const jColor ANIMAL_BABY(0, 0, 100);

// the master from which all Animals are copied... unless there's genetic crossover and mutation.
Animal gMasterAnimal;

// --------------------
Animal::Animal()
    : Sociability(11, 0, 100)
    , ChildMinimumEnergy(9, 0, 52)
    , ParentEnergyThreshold(8, 1, 300)
    , YoungThreshold(0, 0, 300)
    , OldThreshold(1, 0, 300)
    , EnergyToMove(2, 1, 13)
    , PlantsEatenPerDay(3, 1, 13)
    , EnergyGainedByEatingPlants(4, 1, 13)
    , EnergyLossPerDay(5, 1, 13)
    , ExistsPercentage(12, 0, 100)
    , PercentageEnergyLossYoung(6, 0.0, 0.0010)
    , PercentageEnergyLossOld(7, 0.0, 0.0100)
    , ChildPercentageAdditionalEnergy(10, 0.0, 1.0) //
{
    init();
}

// --------------------
void Animal::init()
{
    Type.init();
    Energy = 0;
    Age = 0;
    YoungThreshold.set(Type, 50); // this age and below is "young"
    OldThreshold.set(Type, 200); // this age and above is "old"
    EnergyToMove.set(Type, 8); // the amount of energy to move closer to food
    PlantsEatenPerDay.set(Type, 8); // the amount of plants Animal1 eats
    EnergyGainedByEatingPlants.set(Type, 13); // the amount of energy Animal1 gets by eating plants
    EnergyLossPerDay.set(Type, 7); // the amount of energy Animal1 loses per day
    PercentageEnergyLossYoung.set(Type, 0.0005); // additional energy loss due to youth
    PercentageEnergyLossOld.set(Type, 0.0050); // additional energy loss due to old age
    ParentEnergyThreshold.set(Type, 250); // how much energy Animal1 needs to be a parent
    ChildMinimumEnergy.set(Type, 50); // the minimum amount of energy Animal1 children get when they are born
    ChildPercentageAdditionalEnergy.set(Type, 0.50); // additional energy they get from the parents.
    // set this to 100 to show that socialable is anti-survival!
    // set this to 0 to show a voracious, eating machine.
    Sociability.set(Type, 50); // on sociable the animal is 0..100, 0 is unsociable, 100 is "clingy"
    ExistsPercentage.set(Type, 50); // the percentage of time an Animal1 is initially created
}

// --------------------
Animal& Animal::operator=(const Animal& other)
{
    Type = other.Type;
    Energy = other.Energy;
    Age = other.Age;

    YoungThreshold.set(Type, other.YoungThreshold);
    OldThreshold.set(Type, other.OldThreshold); // this age and above is "old"
    EnergyToMove.set(Type, other.EnergyToMove); // the amount of energy it costs to move an Animal1
    PlantsEatenPerDay.set(Type, other.PlantsEatenPerDay); // the amount of plants Animal1 eats
    EnergyGainedByEatingPlants.set(Type,
        other.EnergyGainedByEatingPlants); // the amount of energy Animal1 gets by eating plants
    EnergyLossPerDay.set(Type, other.EnergyLossPerDay); // the amount of energy Animal1 loses per day
    PercentageEnergyLossYoung.set(Type, other.PercentageEnergyLossYoung); // additional energy loss due to youth
    PercentageEnergyLossOld.set(Type, other.PercentageEnergyLossOld); // additional energy loss due to old age
    ParentEnergyThreshold.set(Type, other.ParentEnergyThreshold); // how much energy Animal1 needs to be a parent
    ChildMinimumEnergy.set(Type,
        other.ChildMinimumEnergy); // the minimum amount of energy Animal1 children get when they are born
    ChildPercentageAdditionalEnergy.set(Type,
        other.ChildPercentageAdditionalEnergy); // additional energy they get from the parents.
    Sociability.set(Type, other.Sociability);
    ExistsPercentage.set(Type, other.ExistsPercentage); // the percentage of time an Animal1 is initially created

    return *this;
}

// --------------------
void Animal::set_initial(const Animal& master)
{
    this->operator=(master);
    int exists = rand() % 100;
    if (exists < ExistsPercentage) {
        Energy = (rand() % 100) + 1;
        Age = (rand() % 600) + 1;
        Type.init();
    }
}

// --------------------
auto Animal::exists() const -> bool
{
    return Energy > 0;
}

// --------------------
void Animal::kill_it()
{
    Type.init();
    Energy = 0;
    Age = 0;
}

// --------------------
void Animal::update(int& currentPlantLife)
{
    // animal ages every day
    Age++;

    if (currentPlantLife >= PlantsEatenPerDay) {
        // there are enough plants to eat...
        currentPlantLife -= PlantsEatenPerDay;
        Energy += EnergyGainedByEatingPlants;
    }

    // animal loses energy every day
    Energy -= EnergyLossPerDay;

    // young animals lose more energy
    if (Age < YoungThreshold) {
        // lose more energy the younger you are...
        Energy -= (int)((YoungThreshold - Age) * Energy * PercentageEnergyLossYoung);
    }

    // old animals lose more energy
    if (Age > OldThreshold) {
        // lose more energy the older you are...
        Energy -= (int)((Age - OldThreshold) * Energy * PercentageEnergyLossOld);
    }

    // if the animal has no energy, it's dead
    if (!exists()) {
        kill_it();
    }
}

// --------------------
auto Animal::get_color() const -> jColor
{
    if (Age < 10) {
        return ANIMAL_BABY;
    }

    if (Energy > 500) {
        return ANIMAL_9;
    }

    if (Energy > 450) {
        return ANIMAL_8;
    }

    if (Energy > 400) {
        return ANIMAL_7;
    }

    if (Energy > 350) {
        return ANIMAL_6;
    }

    if (Energy > 300) {
        return ANIMAL_5;
    }

    if (Energy > 250) {
        return ANIMAL_4;
    }

    if (Energy > 200) {
        return ANIMAL_3;
    }

    if (Energy > 150) {
        return ANIMAL_2;
    }

    if (Energy > 100) {
        return ANIMAL_1;
    }

    if (Energy > 0) {
        return ANIMAL_0;
    }

    // it's dead
    return WHITE;
}

// --------------------
// if occupied
auto Animal::is_higher_then_set(int& energy) const -> bool
{
    if (Energy > energy) {
        energy = Energy;
        return true;
    }
    return false;
}

// --------------------
auto Animal::is_sociable() const -> bool
{
    int dice = rand() % 100;
    return (Sociability > dice) && (Energy >= EnergyToMove);
}

// --------------------
// move if: not enough plantlife and there's enough energy to move
auto Animal::not_enough_food(int currentPlantLife) const -> bool
{
    return (currentPlantLife < PlantsEatenPerDay) && (Energy >= EnergyToMove);
}

// --------------------
void Animal::move(Animal& from)
{
    if (from.Energy < from.EnergyToMove)
        return; // not enough energy to move

    // a move is a copy from the original spot...
    this->operator=(from);

    // remove a bit of energy...
    Energy -= EnergyToMove;
    if (!exists()) {
        kill_it();
    }

    // and remove it from the previous cell
    from.kill_it();
}

// --------------------
// both parents have to have enough energy to have a child
auto Animal::can_be_born(int parent1energy, int parent2energy) const -> bool
{
    bool b = parent1energy >= ParentEnergyThreshold && parent2energy >= ParentEnergyThreshold;
    // std::cout << "CanBeBorn: " << b << std::endl;
    return b;
}

// --------------------
//   - the child's energy is a minimum value + a random extra value, depending on parent's energy
void Animal::be_born(const Animal& parent1, const Animal& parent2)
{
    if (!gEnvironment.UseGenetics) {
        // copy current settings from master animal
        this->operator=(gMasterAnimal);
    }

    // arbitrarily choose parent1 here
    // if genetics are in use, it will be overwritten
    // otherwise, both parents have the same type anyway
    Type = parent1.Type;

    // max energy the child gets is a percentage from each of the parents
    double max = ChildPercentageAdditionalEnergy * parent1.Energy;
    max += ChildPercentageAdditionalEnergy * parent2.Energy;
    Energy = ChildMinimumEnergy;
    Energy += rand() % (int)max; // ranges from 0..max

    // we all start so young...
    Age = 1;

    // since the animal has energy and a non-zero age,
    //   it is officially born at this point

    if (gEnvironment.UseGenetics) {
        // handle crossover from either parents items
        crossover(parent1, parent2);

        // handle mutations
        mutate(parent1, parent2);
    }
}

// --------------------
// the child's energy will be subtracted from each of the parents
void Animal::adjust_parent_for_birth_of(const Animal& child)
{
    // there is no female and male, so each gives up their energy equally
    Energy -= child.Energy / 2;
    if (!exists()) {
        kill_it(); // died in childbirth
    }
}

// --------------------
void Animal::crossover(const Animal& parent1, const Animal& parent2)
{
    YoungThreshold.crossover(Type, parent1.YoungThreshold, parent2.YoungThreshold);
    OldThreshold.crossover(Type, parent1.OldThreshold, parent2.OldThreshold);
    EnergyToMove.crossover(Type, parent1.EnergyToMove, parent2.EnergyToMove);
    PlantsEatenPerDay.crossover(Type, parent1.PlantsEatenPerDay, parent2.PlantsEatenPerDay);
    EnergyGainedByEatingPlants.crossover(Type, parent1.EnergyGainedByEatingPlants, parent2.EnergyGainedByEatingPlants);
    EnergyLossPerDay.crossover(Type, parent1.EnergyLossPerDay, parent2.EnergyLossPerDay);
    PercentageEnergyLossYoung.crossover(Type, parent1.PercentageEnergyLossYoung, parent2.PercentageEnergyLossYoung);
    PercentageEnergyLossOld.crossover(Type, parent1.PercentageEnergyLossOld, parent2.PercentageEnergyLossOld);
    ParentEnergyThreshold.crossover(Type, parent1.ParentEnergyThreshold, parent2.ParentEnergyThreshold);
    ChildMinimumEnergy.crossover(Type, parent1.ChildMinimumEnergy, parent2.ChildMinimumEnergy);
    ChildPercentageAdditionalEnergy.crossover(Type, parent1.ChildPercentageAdditionalEnergy,
        parent2.ChildPercentageAdditionalEnergy);
    Sociability.crossover(Type, parent1.Sociability, parent2.Sociability);
    ExistsPercentage.crossover(Type, parent1.ExistsPercentage, parent2.ExistsPercentage);
}

// --------------------
void Animal::mutate(const Animal& /*parent1*/, const Animal& /*parent2*/)
{
    YoungThreshold.mutate(Type);
    OldThreshold.mutate(Type);
    EnergyToMove.mutate(Type);
    PlantsEatenPerDay.mutate(Type);
    EnergyGainedByEatingPlants.mutate(Type);
    EnergyLossPerDay.mutate(Type);
    PercentageEnergyLossYoung.mutate(Type);
    PercentageEnergyLossOld.mutate(Type);
    ChildMinimumEnergy.mutate(Type);
    ChildPercentageAdditionalEnergy.mutate(Type);
    Sociability.mutate(Type);
    ExistsPercentage.mutate(Type);
}
