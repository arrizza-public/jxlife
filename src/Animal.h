#pragma once

#include "jColor.h"
#include <cstring>
#include <string>

#include "AnimalType.h"
#include "DoubleValue.h"
#include "IntValue.h"

// --------------------
class Animal {
public:
    Animal();
    void init();
    void set_initial(const Animal& master);
    void update(int& currentPlantLife);
    [[nodiscard]] auto can_be_born(int parent1energy, int parent2energy) const -> bool;
    [[nodiscard]] auto exists() const -> bool;
    [[nodiscard]] auto get_color() const -> jColor;
    auto is_higher_then_set(int& energy) const -> bool;
    [[nodiscard]] auto not_enough_food(int currentPlantLife) const -> bool;
    void move(Animal& from);
    [[nodiscard]] auto is_sociable() const -> bool;
    void be_born(const Animal& parent1, const Animal& parent2);
    void adjust_parent_for_birth_of(const Animal& child);
    Animal& operator=(const Animal& other);

    // These are animal characteristics (i.e. "genes")
    AnimalType Type {};
    IntValue Sociability;
    IntValue ChildMinimumEnergy;
    IntValue ParentEnergyThreshold;
    IntValue YoungThreshold;
    IntValue OldThreshold;
    IntValue EnergyToMove;
    IntValue PlantsEatenPerDay;
    IntValue EnergyGainedByEatingPlants;
    IntValue EnergyLossPerDay;
    IntValue ExistsPercentage;
    DoubleValue PercentageEnergyLossYoung;
    DoubleValue PercentageEnergyLossOld;
    DoubleValue ChildPercentageAdditionalEnergy;

private:
    void kill_it();
    void crossover(const Animal& parent1, const Animal& parent2);
    void mutate(const Animal& parent1, const Animal& parent2);

    int Energy {};
    int Age {};
};

extern Animal gMasterAnimal;
