#include "Cell.h"
#include "Animal.h"
#include "EnvironmentInfo.h"
#include "GridInfo.h"

#include <cstdlib>
#include <iostream>

static jColor WHITE(255, 255, 255);
static jColor SUNSHINE(255, 255, 0);

static jColor GREEN9(0, 165, 0);
static jColor GREEN8(0, 175, 0);
static jColor GREEN7(0, 185, 0);
static jColor GREEN6(0, 195, 0);
static jColor GREEN5(0, 205, 0);
static jColor GREEN4(0, 215, 0);
static jColor GREEN3(0, 225, 0);
static jColor GREEN2(0, 235, 0);
static jColor GREEN1(0, 245, 0);
static jColor GREEN0(0, 255, 0);

static jColor DIRT0(240, 230, 140);

// --------------------
Cell::Cell()
    : color(WHITE)
    , m_has_sunshine(false)
    , m_plant_life(0)
    , m_dirt(0)
{
}

// --------------------
void Cell::init_animal()
{
    mAnimal.set_initial(gMasterAnimal);
}

// --------------------
void Cell::set_sunshine()
{
    int sun = rand() % 100;
    if (gEnvironment.SunPercentage > sun) {
        m_has_sunshine = true;
    } else {
        m_has_sunshine = false;
    }
}

// --------------------
jColor Cell::get_color()
{
    return color;
}

// // --------------------
// void Cell::set_color(const jColor &c) {
//     color = c;
// }
//
// // --------------------
// void Cell::inc_color(int inc) {
//     if (inc == 0) {
//         return;
//     }
//     color.red += inc;
//     color.green += inc;
//     color.blue += inc;
// }

// --------------------
void Cell::update_colors()
{
    if (mAnimal.exists()) {
        color = mAnimal.get_color();
    } else if (m_plant_life > 90) {
        color = GREEN9;
    } else if (m_plant_life > 80) {
        color = GREEN8;
    } else if (m_plant_life > 70) {
        color = GREEN7;
    } else if (m_plant_life > 60) {
        color = GREEN6;
    } else if (m_plant_life > 50) {
        color = GREEN5;
    } else if (m_plant_life > 40) {
        color = GREEN4;
    } else if (m_plant_life > 30) {
        color = GREEN3;
    } else if (m_plant_life > 20) {
        color = GREEN2;
    } else if (m_plant_life > 10) {
        color = GREEN1;
    } else if (m_plant_life > 0) {
        color = GREEN0;
    } else if (m_dirt > 0) {
        color = DIRT0;
    } else if (m_has_sunshine) {
        color = SUNSHINE;
    } else {
        color = WHITE;
    }
}

// --------------------
void Cell::update_plant_life()
{
    if (m_has_sunshine) {
        // if there's dirt, plants grow faster
        if (m_dirt > 0) {
            m_plant_life += gEnvironment.PlantLifeIncWithDirt;
            m_dirt--; // use up the dirt
        } else {
            m_plant_life += gEnvironment.PlantLifeIncWithoutDirt;
        }
    } else if (m_plant_life >= gEnvironment.PlantLifeDecWithoutSunshine) {
        // no sunshine, high plantlife
        m_plant_life -= gEnvironment.PlantLifeDecWithoutSunshine;
    } else // no sunshine, plantlife is dead
    {
        // there were some plants, convert it to dirt
        if (m_plant_life > 0) {
            m_dirt++;
        }
        m_plant_life = 0;
    }

    if (m_plant_life > 100) {
        m_plant_life = 100;
    } else if (m_plant_life < 0) {
        m_plant_life = 0;
    }
}

// --------------------
void Cell::update_animal(int row, int col)
{
    // if it's dead, check if a new animal should be created
    if (!mAnimal.exists()) {
        create_new_animal(row, col);
        return;
    }

    // it's alive, so eat and make babies
    mAnimal.update(m_plant_life);

    if (mAnimal.is_sociable()) {
        move_closer_to_friend(row, col);
    } else if (mAnimal.not_enough_food(m_plant_life)) {
        // if current plantlife is low but animal has enough strength to move
        // try to find more food...
        move_closer_to_food(row, col);
    }

    if (mAnimal.exists()) {
        // a live animal produces "dirt" every day
        m_dirt++;
    } else {
        // a dead animal produces a bit more "dirt"
        m_dirt += 2;
    }
}

// --------------------
void Cell::move_closer_to_food(int row, int col)
{
    // move to a vacant neighboring square with the highest green
    int bestrow = -1;
    int bestcol = -1;
    int bestgreen = -1;

    int otherrow;
    int othercol;

    // row above
    otherrow = row - 1;
    othercol = col - 1;
    if (is_better_food(otherrow, othercol, bestgreen)) {
        bestrow = otherrow;
        bestcol = othercol;
    }

    otherrow = row - 1;
    othercol = col;
    if (is_better_food(otherrow, othercol, bestgreen)) {
        bestrow = otherrow;
        bestcol = othercol;
    }

    otherrow = row - 1;
    othercol = col + 1;
    if (is_better_food(otherrow, othercol, bestgreen)) {
        bestrow = otherrow;
        bestcol = othercol;
    }

    // current row
    otherrow = row;
    othercol = col - 1;
    if (is_better_food(otherrow, othercol, bestgreen)) {
        bestrow = otherrow;
        bestcol = othercol;
    }

    otherrow = row;
    othercol = col + 1;
    if (is_better_food(otherrow, othercol, bestgreen)) {
        bestrow = otherrow;
        bestcol = othercol;
    }

    // row below
    otherrow = row + 1;
    othercol = col - 1;
    if (is_better_food(otherrow, othercol, bestgreen)) {
        bestrow = otherrow;
        bestcol = othercol;
    }

    otherrow = row + 1;
    othercol = col;
    if (is_better_food(otherrow, othercol, bestgreen)) {
        bestrow = otherrow;
        bestcol = othercol;
    }

    otherrow = row + 1;
    othercol = col + 1;
    if (is_better_food(otherrow, othercol, bestgreen)) {
        bestrow = otherrow;
        bestcol = othercol;
    }

    // found the best neighboring cell, so move...
    if (bestrow != -1 && bestgreen > 0) {
        // move to the best neighboring cell, but it costs some energy to move...
        Cells[bestrow][bestcol].mAnimal.move(mAnimal);
    }
}

// --------------------
// check if the grass is greener...
bool Cell::is_better_food(int r, int c, int& bestgreen)
{
    if (r >= 0 && r < GridInfo::numrows && // is row in the grid bounds?
        c >= 0 && c < GridInfo::numcols && // is col in the grid bounds?
        !Cells[r][c].mAnimal.exists()) // is the cell empty?
    {
        if (Cells[r][c].m_plant_life > bestgreen) // is the plantlife better than we've ever seen?
        {
            bestgreen = Cells[r][c].m_plant_life;
            return true;
        }
    }

    return false;
}

// --------------------
void Cell::move_closer_to_friend(int row, int col)
{
    int bestrow = -1;
    int bestcol = -1;
    int bestenergy = -1;

    // initialize to the current cell's energy
    // if it's the highest energy in the pack, it doesn't move
    // all the other cells will move closer to it.
    mAnimal.is_higher_then_set(bestenergy);

    int movetorow;
    int movetocol;

    // row above
    movetorow = row - 1;
    movetocol = col - 1;
    if (movetorow >= 0 && movetorow < GridInfo::numrows && // is row in the grid bounds?
        movetocol >= 0 && movetocol < GridInfo::numcols && // is col in the grid bounds?
        !Cells[movetorow][movetocol].mAnimal.exists() && // the space is empty
        Cells[movetorow][movetocol].m_plant_life >= m_plant_life) // and the grass is ok
    {
        if (is_better_friend(row, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }

        if (is_better_friend(row - 1, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }

        if (is_better_friend(row - 2, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }

        if (is_better_friend(row - 2, col - 1, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }

        if (is_better_friend(row - 2, col, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
    }

    movetorow = row - 1;
    movetocol = col;
    if (movetorow >= 0 && movetorow < GridInfo::numrows && // is row in the grid bounds?
        movetocol >= 0 && movetocol < GridInfo::numcols && // is col in the grid bounds?
        !Cells[movetorow][movetocol].mAnimal.exists() && // the space is empty
        Cells[movetorow][movetocol].m_plant_life >= m_plant_life) // and the grass is ok
    {
        if (is_better_friend(row - 2, col - 1, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row - 2, col, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row - 2, col + 1, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
    }

    movetorow = row - 1;
    movetocol = col + 1;
    if (movetorow >= 0 && movetorow < GridInfo::numrows && // is row in the grid bounds?
        movetocol >= 0 && movetocol < GridInfo::numcols && // is col in the grid bounds?
        !Cells[movetorow][movetocol].mAnimal.exists() && // the space is empty
        Cells[movetorow][movetocol].m_plant_life >= m_plant_life) // and the grass is ok
    {
        if (is_better_friend(row - 2, col, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row - 2, col + 1, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }

        if (is_better_friend(row - 2, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row - 1, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
    }

    movetorow = row;
    movetocol = col + 1;
    if (movetorow >= 0 && movetorow < GridInfo::numrows && // is row in the grid bounds?
        movetocol >= 0 && movetocol < GridInfo::numcols && // is col in the grid bounds?
        !Cells[movetorow][movetocol].mAnimal.exists() && // the space is empty
        Cells[movetorow][movetocol].m_plant_life >= m_plant_life) // and the grass is ok
    {
        if (is_better_friend(row - 1, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }

        if (is_better_friend(row + 1, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
    }

    movetorow = row + 1;
    movetocol = col + 1;
    if (movetorow >= 0 && movetorow < GridInfo::numrows && // is row in the grid bounds?
        movetocol >= 0 && movetocol < GridInfo::numcols && // is col in the grid bounds?
        !Cells[movetorow][movetocol].mAnimal.exists() && // the space is empty
        Cells[movetorow][movetocol].m_plant_life >= m_plant_life) // and the grass is ok
    {
        if (is_better_friend(row, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row + 1, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }

        if (is_better_friend(row + 2, col + 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row + 2, col + 1, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row + 2, col, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
    }

    movetorow = row + 1;
    movetocol = col;
    if (movetorow >= 0 && movetorow < GridInfo::numrows && // is row in the grid bounds?
        movetocol >= 0 && movetocol < GridInfo::numcols && // is col in the grid bounds?
        !Cells[movetorow][movetocol].mAnimal.exists() && // the space is empty
        Cells[movetorow][movetocol].m_plant_life >= m_plant_life) // and the grass is ok
    {
        if (is_better_friend(row + 2, col + 1, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row + 2, col, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row + 2, col - 1, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
    }

    movetorow = row + 1;
    movetocol = col - 1;
    if (movetorow >= 0 && movetorow < GridInfo::numrows && // is row in the grid bounds?
        movetocol >= 0 && movetocol < GridInfo::numcols && // is col in the grid bounds?
        !Cells[movetorow][movetocol].mAnimal.exists() && // the space is empty
        Cells[movetorow][movetocol].m_plant_life >= m_plant_life) // and the grass is ok
    {
        if (is_better_friend(row + 2, col, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row + 2, col - 1, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }

        if (is_better_friend(row + 2, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row + 1, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
    }

    movetorow = row;
    movetocol = col - 1;
    if (movetorow >= 0 && movetorow < GridInfo::numrows && // is row in the grid bounds?
        movetocol >= 0 && movetocol < GridInfo::numcols && // is col in the grid bounds?
        !Cells[movetorow][movetocol].mAnimal.exists() && // the space is empty
        Cells[movetorow][movetocol].m_plant_life >= m_plant_life) // and the grass is ok
    {
        if (is_better_friend(row + 1, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
        if (is_better_friend(row - 1, col - 2, bestenergy)) {
            bestrow = movetorow;
            bestcol = movetocol;
        }
    }

    // found the best neighboring cell, so move...
    if (bestrow != -1 && bestenergy > 0) {
        // std::cout << "move " << row << "," << col << " to " << bestrow << "," << bestcol << std::endl;
        // move closer to the friend cell, but it costs some energy to move...
        Cells[bestrow][bestcol].mAnimal.move(mAnimal);
    }
}

// --------------------
bool Cell::is_better_friend(int friendr, int friendc, int& bestenergy)
{
    return friendr >= 0 && friendr < GridInfo::numrows && // is row in the grid bounds?
        friendc >= 0 && friendc < GridInfo::numcols && // is col in the grid bounds?
        Cells[friendr][friendc].mAnimal.exists() && // friend exists
        Cells[friendr][friendc].mAnimal.is_higher_then_set(bestenergy);
}

// --------------------
void Cell::create_new_animal(int row, int col)
{
    // create a new animal if there are:
    //   - two occupied bordering cells
    //   - both with energy above a threshold
    //   - choose parents with the highest energy
    //   - the child's energy is a minimum value + a random extra value dependant on parent's energy
    //   - the child's energy will be subtracted from the parents

    // find two parents in occupied neighboring squares
    int parent1row = -1;
    int parent1col = -1;
    int parent1energy = -1;

    int parent2row = -1;
    int parent2col = -1;
    int parent2energy = -1;

    int otherrow;
    int othercol;

    //    >be1 &&  >be2 choose be1
    //    >be1 && <=be2 choose be1
    //   <=be1 &&  >be2 choose be2
    //   <=be1 && <=be2 skip
    // row above
    otherrow = row - 1;
    othercol = col - 1;
    if (is_better_parent(otherrow, othercol, parent1energy)) {
        parent1row = otherrow;
        parent1col = othercol;
    } else if (is_better_parent(otherrow, othercol, parent2energy)) {
        parent2row = otherrow;
        parent2col = othercol;
    }

    otherrow = row - 1;
    othercol = col;
    if (is_better_parent(otherrow, othercol, parent1energy)) {
        parent1row = otherrow;
        parent1col = othercol;
    } else if (is_better_parent(otherrow, othercol, parent2energy)) {
        parent2row = otherrow;
        parent2col = othercol;
    }

    otherrow = row - 1;
    othercol = col + 1;
    if (is_better_parent(otherrow, othercol, parent1energy)) {
        parent1row = otherrow;
        parent1col = othercol;
    } else if (is_better_parent(otherrow, othercol, parent2energy)) {
        parent2row = otherrow;
        parent2col = othercol;
    }

    // current row
    otherrow = row;
    othercol = col - 1;
    if (is_better_parent(otherrow, othercol, parent1energy)) {
        parent1row = otherrow;
        parent1col = othercol;
    } else if (is_better_parent(otherrow, othercol, parent2energy)) {
        parent2row = otherrow;
        parent2col = othercol;
    }

    otherrow = row;
    othercol = col + 1;
    if (is_better_parent(otherrow, othercol, parent1energy)) {
        parent1row = otherrow;
        parent1col = othercol;
    } else if (is_better_parent(otherrow, othercol, parent2energy)) {
        parent2row = otherrow;
        parent2col = othercol;
    }

    // row below
    otherrow = row + 1;
    othercol = col - 1;
    if (is_better_parent(otherrow, othercol, parent1energy)) {
        parent1row = otherrow;
        parent1col = othercol;
    } else if (is_better_parent(otherrow, othercol, parent2energy)) {
        parent2row = otherrow;
        parent2col = othercol;
    }

    otherrow = row + 1;
    othercol = col;
    if (is_better_parent(otherrow, othercol, parent1energy)) {
        parent1row = otherrow;
        parent1col = othercol;
    } else if (is_better_parent(otherrow, othercol, parent2energy)) {
        parent2row = otherrow;
        parent2col = othercol;
    }

    otherrow = row + 1;
    othercol = col + 1;
    if (is_better_parent(otherrow, othercol, parent1energy)) {
        parent1row = otherrow;
        parent1col = othercol;
    } else if (is_better_parent(otherrow, othercol, parent2energy)) {
        parent2row = otherrow;
        parent2col = othercol;
    }

    // check if both parents were found...
    // if they are above the threshold, then the cell is occupied
    if (mAnimal.can_be_born(parent1energy, parent2energy)) {
        mAnimal.be_born(Cells[parent1row][parent1col].mAnimal, Cells[parent2row][parent2col].mAnimal);
        Cells[parent1row][parent1col].mAnimal.adjust_parent_for_birth_of(mAnimal);
        Cells[parent2row][parent2col].mAnimal.adjust_parent_for_birth_of(mAnimal);
    }
}

// --------------------
bool Cell::is_better_parent(int r, int c, int& bestenergy)
{
    return r >= 0 && r < GridInfo::numrows && // is row in the grid bounds?
        c >= 0 && c < GridInfo::numcols && // is col in the grid bounds?
        Cells[r][c].mAnimal.exists() && Cells[r][c].mAnimal.is_higher_then_set(bestenergy);
}
