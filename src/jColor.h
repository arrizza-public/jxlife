#pragma once

// --------------------
struct jColor {
    int red;
    int green;
    int blue;

    // --------------------
    jColor()
        : red(0)
        , green(0)
        , blue(0) // default to black
    {
    }

    // --------------------
    jColor(unsigned char r, unsigned char g, unsigned char b)
        : red(r)
        , green(g)
        , blue(b)
    {
    }

    // --------------------
    jColor& operator=(const jColor& other)
    {
        red = other.red;
        green = other.green;
        blue = other.blue;
        return *this;
    }

    // --------------------
    auto operator!=(const jColor& other) const -> bool
    {
        if (red != other.red) {
            return true;
        }
        if (green != other.green) {
            return true;
        }
        if (blue != other.blue) {
            return true;
        }
        return false;
    }
};
