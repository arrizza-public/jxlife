#include "IntParam.h"

// --------------------
std::ostream& operator<<(std::ostream& os, const IntParam& p)
{
    os << p.mValue;
    return os;
}
