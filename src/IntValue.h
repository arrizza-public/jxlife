#pragma once

#include "IntParam.h"

// --------------------
class IntValue : public IntParam {
public:
    // --------------------
    IntValue(int i, int minval, int maxval)
        : IntParam(0, minval, maxval)
        , mIndex(i)
    {
    }

    // --------------------
    void set(AnimalType& type, const int value)
    {
        mValue = value;
        type.set_at(mIndex, (int)'A' + (int)((26.0 * (value - mMin)) / (mMax - mMin)));
    }

    // --------------------
    void set(AnimalType& type, const IntValue& other)
    {
        set(type, other.mValue);
    }

    // --------------------
    void crossover(AnimalType& /*type*/, const IntValue& parent1, const IntValue& parent2)
    {
        int dice;
        dice = rand() % 100;
        mValue = dice > 50 ? parent1.mValue : parent2.mValue;
    }

    // --------------------
    void mutate(AnimalType& type)
    {
        int dice = rand() % 100;
        if (dice > 98) {
            if (mValue < mMax) {
                set(type, mValue + 1);
            }
        } else if (dice < 2) {
            if (mValue > mMin) {
                set(type, mValue - 1);
            }
        }
    }

private:
    int mIndex;

    IntValue();
};
