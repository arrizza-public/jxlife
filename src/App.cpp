#include "App.h"
#include "Animal.h"
#include "Grid.h"
#include "GridInfo.h"
#include <GL/glut.h>
#include <iostream>
#include <pthread.h>
#include <sstream>
#include <unistd.h>
#include <vector>

static App* app;

// --------------------
std::ostream& operator<<(std::ostream& os, const AnimalType& a)
{
    a.print_on(os);
    return os;
}

// --------------------
App::App()
{
    m_generation = 0;
    app = this;
}

// --------------------
App::~App() = default;

// TODO delete?
// int App::onMenuGoStop(const std::string& name)
//  {
//  std::cout << "OnMenuGoStop name=" << name << std::endl;
//
//  if (name == "Go!")
//    {
//    mSettingId->SetEnabled(false);
//    mWin.PostEvent(1, 22, 33);
//    mGoStopId->SetMenuItemText("Stop!");
//    mCalcThread->Unfreeze();
//    }
//  else
//    {
//    mSettingId->SetEnabled(true);
//    mGoStopId->SetMenuItemText("Go!");
//    mWin.PostEvent(1, 0, 0);
//    mCalcThread->Freeze();
//    }
//
//  return 0;
//  }

int win_w = 0.0;
int win_h = 0.0;

// --------------------
int get_extent(const std::string& s)
{
    int extent = 0;
    for (char i : s) {
        extent += glutBitmapWidth(GLUT_BITMAP_HELVETICA_12, i);
    }
    return extent;
}

// --------------------
void output(int x, int y, const char* string)
{
    glColor3f(0.0, 0.0, 0.0);

    glRasterPos2i(x, y);
    int len = (int)::strlen(string);
    for (int i = 0; i < len; ++i) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, string[i]);
    }
}

// --------------------
template <typename T>
void draw_label_and_value(const jRect& rct, const std::string& label, T value)
{
    std::stringstream s;
    s << label << value;
    output(rct.x, rct.y, s.str().c_str());
}

// --------------------
template <typename T>
void draw_label_and_bool(const jRect& rct, const std::string& label, T value)
{
    std::stringstream s;
    s << label << std::boolalpha << value;
    output(rct.x, rct.y, s.str().c_str());
}

// TODO delete
// // --------------------
// void draw_label(const jRect &rct, const std::string &label) {
//     output(rct.x, rct.y, label.c_str());
// }

// --------------------
void draw_bordered_label(const jRect& rct, const std::string& label)
{
    int extent = get_extent(label);
    int gap = (rct.width - extent) / 2;
    if (gap < 0) {
        gap = 0;
    }
    output(rct.x + gap, rct.y + 5, label.c_str());
    glBegin(GL_LINE_LOOP);
    glVertex2i(rct.x, rct.y);
    glVertex2i(rct.x, rct.y + rct.height);
    glVertex2i(rct.x + rct.width, rct.y + rct.height);
    glVertex2i(rct.x + rct.width, rct.y);
    glEnd();
}

bool load_buttons = true;
typedef std::vector<std::pair<jRect, int>> ButtonList;
ButtonList buttons;
#define BTN_REINIT 1
#define BTN_SunPercentage_UP 2
#define BTN_SunPercentage_DOWN 3
#define BTN_Socialability_UP 4
#define BTN_Socialability_DOWN 5
#define BTN_ChildMinimumEnergy_UP 6
#define BTN_ChildMinimumEnergy_DOWN 7
#define BTN_PlantLifeIncWithDirt_UP 8
#define BTN_PlantLifeIncWithDirt_DOWN 9
#define BTN_PlantLifeIncWithoutDirt_UP 10
#define BTN_PlantLifeIncWithoutDirt_DOWN 11
#define BTN_PlantLifeDecWithoutSunshine_UP 12
#define BTN_PlantLifeDecWithoutSunshine_DOWN 13
#define BTN_ParentEnergyThreshold_UP 14
#define BTN_ParentEnergyThreshold_DOWN 15
#define BTN_YoungThreshold_UP 16
#define BTN_YoungThreshold_DOWN 17
#define BTN_OldThreshold_UP 18
#define BTN_OldThreshold_DOWN 19
// #define BTN_PercentageEnergyLossYoung_UP 20
// #define BTN_PercentageEnergyLossYoung_DOWN 21
// #define BTN_PercentageEnergyLossOld_UP 22
// #define BTN_PercentageEnergyLossOld_DOWN 23
// #define BTN_ChildPercentageAdditionalEnergy_UP 24
// #define BTN_ChildPercentageAdditionalEnergy_DOWN 25
#define BTN_EnergyToMove_UP 26
#define BTN_EnergyToMove_DOWN 27
#define BTN_EnergyGainedByEatingPlants_UP 28
#define BTN_EnergyGainedByEatingPlants_DOWN 29
#define BTN_EnergyLossPerDay_UP 30
#define BTN_EnergyLossPerDay_DOWN 31
#define BTN_PlantsEatenPerDay_UP 32
#define BTN_PlantsEatenPerDay_DOWN 33
#define BTN_ExistsPercentage_UP 34
#define BTN_ExistsPercentage_DOWN 35
#define BTN_UseGenetics_BOOL 36

// --------------------
void draw_button(const jRect& rct, const std::string& label, int id)
{
    if (load_buttons) {
        buttons.emplace_back(rct, id);
    }
    draw_bordered_label(rct, label);
}

#define LINEHEIGHT 18
#define BUTTONGAP 15
#define GAP 15

// --------------------
template <typename T>
void draw_parm(jRect& rct, int up, const std::string& label, int down, T value)
{
    rct.x = (GridInfo::numcols * GridInfo::colwidth) + GAP;
    rct.width = 13;
    draw_button(rct, "^", up);
    rct.x += rct.width + 5;
    rct.width = 130;
    draw_bordered_label(rct, label);
    rct.x += rct.width + 5;
    rct.width = 13;
    draw_button(rct, "v", down);
    rct.x = (GridInfo::numcols * GridInfo::colwidth) + GAP + 180;
    draw_label_and_value(rct, " ", value);
    rct.y -= rct.height + 2; // next line
}

// --------------------
template <typename T>
void draw_parm_bool(jRect& rct, int toggle, const std::string& label, T value)
{
    rct.x = (GridInfo::numcols * GridInfo::colwidth) + GAP;
    rct.width = 13;
    // skip

    rct.x += rct.width + 5;
    rct.width = 130;
    draw_bordered_label(rct, label);

    rct.x += rct.width + 5;
    rct.width = 13;
    draw_button(rct, "!", toggle);

    rct.x = (GridInfo::numcols * GridInfo::colwidth) + GAP + 180;
    draw_label_and_bool(rct, " ", value);
    rct.y -= rct.height + 2; // next line
}

// --------------------
void App::display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glOrtho(0, win_w, 0, win_h, -1, 1);

    AnimalMap count;
    Grid::paint(count);
    jRect rct {};
    rct.x = 10;
    rct.y = (GridInfo::numrows * GridInfo::rowheight) + 20;
    rct.height = LINEHEIGHT;

    // the menu along the top
    rct.width = 60;
    draw_button(rct, "Re-init!", BTN_REINIT);
    rct.x += rct.width + BUTTONGAP; // next button
    //  MenuCallBack gostopcmd(this, &App::OnMenuGoStop);
    //  mGoStopId = mMenu.CreateMenuItem("Go!", gostopcmd);

    // data to the right of the grid
    rct.x = (GridInfo::numcols * GridInfo::colwidth) + GAP;
    rct.y = (GridInfo::numrows * GridInfo::rowheight) - LINEHEIGHT;
    rct.height = LINEHEIGHT;
    rct.width = 300;

    // === environment related
    draw_parm(rct, BTN_SunPercentage_UP, "Sun %", BTN_SunPercentage_DOWN, gEnvironment.SunPercentage);
    draw_parm(rct, BTN_PlantLifeIncWithDirt_UP, "Inc Plant w/ Dirt", BTN_PlantLifeIncWithDirt_DOWN,
        gEnvironment.PlantLifeIncWithDirt);
    draw_parm(rct, BTN_PlantLifeIncWithoutDirt_UP, "Inc Plant w/o Dirt", BTN_PlantLifeIncWithoutDirt_DOWN,
        gEnvironment.PlantLifeIncWithoutDirt);
    draw_parm(rct, BTN_PlantLifeDecWithoutSunshine_UP, "Dec Plant w/o Sun", BTN_PlantLifeDecWithoutSunshine_DOWN,
        gEnvironment.PlantLifeDecWithoutSunshine);
    // === animal related
    draw_parm(rct, BTN_Socialability_UP, "Socialability", BTN_Socialability_DOWN, gMasterAnimal.Sociability);
    draw_parm(rct, BTN_YoungThreshold_UP, "Young Thresh.", BTN_YoungThreshold_DOWN,
        gMasterAnimal.YoungThreshold);
    draw_parm(rct, BTN_OldThreshold_UP, "Old Thresh.", BTN_OldThreshold_DOWN,
        gMasterAnimal.OldThreshold);
    draw_parm(rct, BTN_ChildMinimumEnergy_UP, "Child Min. Energy", BTN_ChildMinimumEnergy_DOWN,
        gMasterAnimal.ChildMinimumEnergy);
    draw_parm(rct, BTN_ParentEnergyThreshold_UP, "Parent Energy Thresh.", BTN_ParentEnergyThreshold_DOWN,
        gMasterAnimal.ParentEnergyThreshold);

    // TODO handle doubles
    // draw_parm(rct, BTN_PercentageEnergyLossYoung_UP, "% Energy Loss-Child", BTN_PercentageEnergyLossYoung_DOWN,
    //           gMasterAnimal.PercentageEnergyLossYoung);
    // draw_parm(rct, BTN_PercentageEnergyLossOld_UP, "% Energy Low-Old", BTN_PercentageEnergyLossOld_DOWN,
    //           gMasterAnimal.PercentageEnergyLossOld);
    // draw_parm(rct, BTN_ChildPercentageAdditionalEnergy_UP, "% Add. Energy Child",
    //           BTN_ChildPercentageAdditionalEnergy_DOWN,
    //           gMasterAnimal.ChildPercentageAdditionalEnergy);

    draw_parm(rct, BTN_EnergyToMove_UP, "Energy to Move", BTN_EnergyToMove_DOWN,
        gMasterAnimal.EnergyToMove);
    draw_parm(rct, BTN_EnergyGainedByEatingPlants_UP, "Energy Gained Eating", BTN_EnergyGainedByEatingPlants_DOWN,
        gMasterAnimal.EnergyGainedByEatingPlants);
    draw_parm(rct, BTN_EnergyLossPerDay_UP, "Energy Loss/day", BTN_EnergyLossPerDay_DOWN,
        gMasterAnimal.EnergyLossPerDay);

    draw_parm(rct, BTN_PlantsEatenPerDay_UP, "Plants eaten/day", BTN_PlantsEatenPerDay_DOWN,
        gMasterAnimal.PlantsEatenPerDay);
    draw_parm(rct, BTN_ExistsPercentage_UP, "% exists", BTN_ExistsPercentage_DOWN,
        gMasterAnimal.ExistsPercentage);

    draw_parm_bool(rct, BTN_UseGenetics_BOOL, "Use Genetics", gEnvironment.UseGenetics);

    rct.y -= 10; // next line

    rct.x = (GridInfo::numcols * GridInfo::colwidth) + GAP;
    m_generation++;
    draw_label_and_value(rct, "Generation: ", m_generation);
    rct.y -= rct.height; // next line

    draw_label_and_value(rct, "#Types: ", count.size());
    rct.y -= rct.height; // next line

    int population = 0;
    for (auto& it : count) {
        population += it.second;
    }
    draw_label_and_value(rct, "Population: ", population);
    rct.y -= rct.height; // next line

    // print the top n animals
#define MAXROWSTOPRINT 10
    int rowsprinted = 0;
    int mostsofar = 9999999;
    bool foundone;
    int bestpop;
    AnimalType besttype {};
    do {
        bestpop = -1;
        besttype.init();
        foundone = false;
        for (auto& it : count) {
            if (it.second > bestpop && it.second < mostsofar) {
                foundone = true;
                besttype = it.first;
                bestpop = it.second;
            }
        }

        if (foundone) {
            mostsofar = bestpop;

            rowsprinted++;
            std::stringstream s;
            s << "Type: " << besttype << " Pop: ";
            draw_label_and_value(rct, s.str(), bestpop);
            rct.y -= rct.height; // next line
        }
    } while (foundone && rowsprinted < MAXROWSTOPRINT);

    while (rowsprinted < MAXROWSTOPRINT) {
        rowsprinted++;
        draw_label_and_value(rct, "-", "");
        rct.y -= rct.height; // next line
    }

    rct.y -= rct.height; // next line

    load_buttons = false;
    glutSwapBuffers();
}

// --------------------
void reshape(int w, int h)
{
    win_w = w;
    win_h = h;
    glViewport(0, 0, w, h);
}

// --------------------
static pthread_t tid = 0;

[[noreturn]] void* calc_thread(void* /*arg*/)
{
    for (;;) {
        Grid::calculate();
    }
}

static int lasttime = 0L;

// --------------------
void idle()
{
    if (tid == 0) {
        pthread_create(&tid, nullptr, calc_thread, nullptr);
        ::usleep(100);
    }

    const int timePerFrame = 20; // ms
    int t = glutGet(GLUT_ELAPSED_TIME);
    int delay = timePerFrame - (t - lasttime);
    if (delay > 0) {
        ::usleep(delay * 1000);
    }
    glutPostRedisplay();
    lasttime = glutGet(GLUT_ELAPSED_TIME);
}

// --------------------
void on_mouse(int button, int state, int x, int y)
{
    y = win_h - y;
    std::cout << "win_h=" << win_h << "\n";
    std::cout << "on_mouse btn=" << button << " state= " << state << " x=" << x << " y=" << y << std::endl;
    if (state == 0)
        return;

    // btn  : 0 => left; 2 => right
    // state: 0 => down; 1 => up
    // x y : coordinates
    int id = 0;
    for (const auto& it : buttons) {
        jRect rct = it.first;
        std::cout << "on_mouse check " << it.second
                  << ": rct.x=" << rct.x << " rct.y=" << rct.y
                  << " rct.width=" << rct.width
                  << " rct.height=" << rct.height << "\n";
        if (x >= rct.x && x <= rct.x + rct.width && y >= rct.y && y <= rct.y + rct.height) {
            id = it.second;
            break;
        }
    }
    std::cout << "on_mouse: done\n";
    switch (id) {
    case 0:
        break;

    case BTN_REINIT:
        Grid::reinit();
        break;

    case BTN_SunPercentage_UP:
        ++gEnvironment.SunPercentage;
        break;

    case BTN_SunPercentage_DOWN:
        --gEnvironment.SunPercentage;
        break;

    case BTN_PlantLifeIncWithDirt_UP:
        ++gEnvironment.PlantLifeIncWithDirt;
        break;

    case BTN_PlantLifeIncWithDirt_DOWN:
        --gEnvironment.PlantLifeIncWithDirt;
        break;

    case BTN_PlantLifeIncWithoutDirt_UP:
        ++gEnvironment.PlantLifeIncWithoutDirt;
        break;

    case BTN_PlantLifeIncWithoutDirt_DOWN:
        --gEnvironment.PlantLifeIncWithoutDirt;
        break;

    case BTN_PlantLifeDecWithoutSunshine_UP:
        ++gEnvironment.PlantLifeDecWithoutSunshine;
        break;

    case BTN_PlantLifeDecWithoutSunshine_DOWN:
        --gEnvironment.PlantLifeDecWithoutSunshine;
        break;

    case BTN_Socialability_UP:
        ++gMasterAnimal.Sociability;
        break;

    case BTN_Socialability_DOWN:
        --gMasterAnimal.Sociability;
        break;

    case BTN_ChildMinimumEnergy_UP:
        ++gMasterAnimal.ChildMinimumEnergy;
        break;

    case BTN_ChildMinimumEnergy_DOWN:
        --gMasterAnimal.ChildMinimumEnergy;
        break;

    case BTN_ParentEnergyThreshold_UP:
        ++gMasterAnimal.ParentEnergyThreshold;
        break;

    case BTN_ParentEnergyThreshold_DOWN:
        --gMasterAnimal.ParentEnergyThreshold;
        break;

    case BTN_YoungThreshold_UP:
        ++gMasterAnimal.YoungThreshold;
        break;

    case BTN_YoungThreshold_DOWN:
        --gMasterAnimal.YoungThreshold;
        break;

    case BTN_OldThreshold_UP:
        ++gMasterAnimal.OldThreshold;
        break;

    case BTN_OldThreshold_DOWN:
        --gMasterAnimal.OldThreshold;
        break;

        // TOOD handle Doubles
        // case BTN_PercentageEnergyLossYoung_UP:
        //     ++gMasterAnimal.PercentageEnergyLossYoung;
        //     break;
        //
        // case BTN_PercentageEnergyLossYoung_DOWN:
        //     --gMasterAnimal.PercentageEnergyLossYoung;
        //     break;
        //
        // case BTN_PercentageEnergyLossOld_UP:
        //     ++gMasterAnimal.PercentageEnergyLossOld;
        //     break;
        //
        // case BTN_PercentageEnergyLossOld_DOWN:
        //     --gMasterAnimal.PercentageEnergyLossOld;
        //     break;
        //
        // case BTN_ChildPercentageAdditionalEnergy_UP:
        //     ++gMasterAnimal.ChildPercentageAdditionalEnergy;
        //     break;
        //
        // case BTN_ChildPercentageAdditionalEnergy_DOWN:
        //     --gMasterAnimal.ChildPercentageAdditionalEnergy;
        //     break;

    case BTN_EnergyToMove_UP:
        ++gMasterAnimal.EnergyToMove;
        break;

    case BTN_EnergyToMove_DOWN:
        --gMasterAnimal.EnergyToMove;
        break;

    case BTN_EnergyGainedByEatingPlants_UP:
        ++gMasterAnimal.EnergyGainedByEatingPlants;
        break;

    case BTN_EnergyGainedByEatingPlants_DOWN:
        --gMasterAnimal.EnergyGainedByEatingPlants;
        break;

    case BTN_EnergyLossPerDay_UP:
        ++gMasterAnimal.EnergyLossPerDay;
        break;

    case BTN_EnergyLossPerDay_DOWN:
        --gMasterAnimal.EnergyLossPerDay;
        break;

    case BTN_PlantsEatenPerDay_UP:
        ++gMasterAnimal.PlantsEatenPerDay;
        break;

    case BTN_PlantsEatenPerDay_DOWN:
        --gMasterAnimal.PlantsEatenPerDay;
        break;

    case BTN_ExistsPercentage_UP:
        ++gMasterAnimal.ExistsPercentage;
        break;

    case BTN_ExistsPercentage_DOWN:
        --gMasterAnimal.ExistsPercentage;
        break;

    case BTN_UseGenetics_BOOL:
        gEnvironment.UseGenetics = !gEnvironment.UseGenetics;
        break;

    default:
        break;
    }
}

// --------------------
void on_display()
{
    app->display();
}

// --------------------
void App::run(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

    glutInitWindowSize((GridInfo::colwidth * GridInfo::numcols) + 275, (GridInfo::rowheight * GridInfo::numrows) + 50);
    glutCreateWindow("jLife - new game of Life");
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glMatrixMode(GL_PROJECTION);

    glLoadIdentity();
    // proj = identity.Ortho(0, width, height, 0, -1.0f, 1.0f);
    glOrtho(0, win_w, win_h, 0, -1, 1);
    glColor3f(0.0, 0.0, 1.0);

    m_generation = 0;
    Grid::init();

    glutDisplayFunc(::on_display);
    glutReshapeFunc(::reshape);
    glutIdleFunc(::idle);
    glutMouseFunc(::on_mouse);

    // this call does not return until the main window is shut down
    glutMainLoop();
}
