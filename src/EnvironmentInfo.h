#pragma once

#include "IntParam.h"

// --------------------
class EnvironmentInfo {
public:
    EnvironmentInfo();
    void init();

    IntParam SunPercentage;
    IntParam PlantLifeIncWithDirt;
    IntParam PlantLifeIncWithoutDirt;
    IntParam PlantLifeDecWithoutSunshine;
    bool UseGenetics;
};

extern EnvironmentInfo gEnvironment;
