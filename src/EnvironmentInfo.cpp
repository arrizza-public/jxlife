#include "EnvironmentInfo.h"

EnvironmentInfo gEnvironment;

// --------------------
EnvironmentInfo::EnvironmentInfo()
    : SunPercentage(32, 1, 100)
    , PlantLifeIncWithDirt(6, 1, 20)
    , PlantLifeIncWithoutDirt(3, 1, 20)
    , PlantLifeDecWithoutSunshine(1, 1, 20)
    , UseGenetics(false)
{
    init();
}

// --------------------
void EnvironmentInfo::init()
{
    SunPercentage = 32; // the percentage of time the sun is shining in a cell
    PlantLifeIncWithDirt = 6; // the plant life increment with sun & dirt
    PlantLifeIncWithoutDirt = 3; // the plant life increment with sun & no dirt
    PlantLifeDecWithoutSunshine = 1; // the plant life decrement with no sun & no dirt
    UseGenetics = false; // do not use genetics for birthing babies
}
