#include "Grid.h"
#include "Animal.h"
#include "EnvironmentInfo.h"
#include "GridInfo.h"

#include <GL/glut.h>
#include <iostream>

// --------------------
// The grid
Cell Cells[GridInfo::numrows][GridInfo::numcols];

// --------------------
void Grid::init()
{
    std::cout << "Grid::init " << std::endl;

    init_environment();
    reinit();
}

// --------------------
void Grid::reinit()
{
    std::cout << "Grid::reinit " << std::endl;

    init_grid();
    init_animal();
    calculate();
}

// --------------------
void Grid::init_environment()
{
    // init the environment
    gEnvironment.init();
    gMasterAnimal.init();
}

// --------------------
void Grid::calculate()
{
    // std::cout << "x=Grid::Calculate " << std::endl;
    update_sunshine();
    update_plant_life();
    update_animal();
    update_colors();
}

// --------------------
// update colors
void Grid::update_colors()
{
    for (int col = 0; col < GridInfo::numcols; ++col) {
        for (auto& Cell : Cells) {
            Cell[col].update_colors();
        }
    }
}

// --------------------
// update colors
void Grid::init_animal()
{
    for (int col = 0; col < GridInfo::numcols; ++col) {
        for (auto& Cell : Cells) {
            Cell[col].init_animal();
        }
    }
}

// --------------------
// update colors
void Grid::update_animal()
{
    for (int col = 0; col < GridInfo::numcols; ++col) {
        for (int row = 0; row < GridInfo::numrows; ++row) {
            Cells[row][col].update_animal(row, col);
        }
    }
}

// --------------------
// update colors
void Grid::update_plant_life()
{
    for (int col = 0; col < GridInfo::numcols; ++col) {
        for (auto& Cell : Cells) {
            Cell[col].update_plant_life();
        }
    }
}

// --------------------
// update sunshine
void Grid::update_sunshine()
{
    for (int col = 0; col < GridInfo::numcols; ++col) {
        for (auto& Cell : Cells) {
            Cell[col].set_sunshine();
        }
    }
}

// --------------------
void Grid::init_grid()
{
    jRect rct {};
    rct.x = 1;
    rct.y = 1;
    rct.width = GridInfo::colwidth - 1;
    rct.height = GridInfo::rowheight - 1;

    // set up the grid
    for (int col = 0; col < GridInfo::numcols; ++col) {
        for (int row = 0; row < GridInfo::numrows; ++row) {
            // calculate cell size
            rct.x = (col * GridInfo::colwidth) + 1;
            rct.y = (row * GridInfo::rowheight) + 1;
            Cells[row][col].rect = rct;
        }
    }
}

// --------------------
// paint the current grid
void Grid::paint(AnimalMap& count)
{
    for (int col = 0; col < GridInfo::numcols; ++col) {
        for (auto& Cell : Cells) {
            jColor c = Cell[col].get_color();
            glColor3f((float)c.red / 255.0F, (float)c.green / 255.0F, (float)c.blue / 255.0F);

            jRect rct = Cell[col].rect;
            glRecti(rct.x, rct.y, rct.x + rct.width, rct.y + rct.height);

            if (Cell[col].mAnimal.exists()) {
                count[Cell[col].mAnimal.Type] = count[Cell[col].mAnimal.Type] + 1;
            }
        }
    }
}
