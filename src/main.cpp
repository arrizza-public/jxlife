#include "App.h"
#include <iostream>

// --------------------
auto main(int argc, char** argv) -> int
{
    std::srand((unsigned int)time(nullptr));

    App app;
    app.run(argc, argv);
    return 0;
}
