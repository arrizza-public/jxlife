#pragma once

// --------------------
class App {
public:
    App();
    ~App();
    void run(int argc, char** argv);
    void display();

private:
    int m_generation;
};
