#pragma once

#include "Animal.h"
#include "EnvironmentInfo.h"
#include "jColor.h"
#include "jRect.h"

class jGraphics;

// --------------------
class Grid {
public:
    static void init();
    static void reinit();
    static void calculate();
    static void paint(AnimalMap& count);

private:
    static void init_environment();
    static void init_grid();
    static void init_animal();
    static void update_colors();
    static void update_sunshine();
    static void update_plant_life();
    static void update_animal();
};
