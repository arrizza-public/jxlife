#pragma once

#include <map>

// --------------------
class AnimalType {
public:
    // TODO delete?
    // // --------------------
    // const std::string get() {
    //     return std::string(m_type);
    // }

    // --------------------
    AnimalType& operator=(const AnimalType& other)
    {
        strcpy(m_type, other.m_type);
        return *this;
    }

    // --------------------
    bool operator<(const AnimalType& other) const
    {
        return strcmp(m_type, other.m_type) < 0;
    }

    // --------------------
    void set_at(int index, int val)
    {
        m_type[index] = (char)val;
    }

    // --------------------
    void print_on(std::ostream& os) const
    {
        os << std::string(m_type);
    }

#define DEADANIMAL "------------"

    // --------------------
    void init()
    {
        strcpy(m_type, DEADANIMAL);
    }

    // TODO delete?
    // // --------------------
    // bool is_dead() {
    //     return strcmp(m_type, DEADANIMAL) == 0;
    // }

private:
    char m_type[14];
};

typedef std::map<AnimalType, int> AnimalMap;
