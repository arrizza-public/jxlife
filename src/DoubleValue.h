#pragma once

// --------------------
class DoubleValue {
public:
    // --------------------
    DoubleValue(int i, double minval, double maxval)
        : mIndex(i)
        , mValue(0.0)
        , mMin(minval)
        , mMax(maxval)
    {
    }

    // --------------------
    operator double() const
    {
        return mValue;
    }

    // --------------------
    void set(AnimalType& type, const double value)
    {
        mValue = value;
        type.set_at(mIndex, 'A' + (char)((26.0 * (value - mMin)) / (mMax - mMin)));
    }

    // --------------------
    void set(AnimalType& type, const DoubleValue& other)
    {
        set(type, other.mValue);
    }

    // --------------------
    void crossover(AnimalType& /*type*/, const DoubleValue& parent1, const DoubleValue& parent2)
    {
        int dice;
        dice = rand() % 100;
        mValue = dice > 50 ? parent1.mValue : parent2.mValue;
    }

    // --------------------
    void mutate(AnimalType& type)
    {
        int dice = rand() % 100;
        if (dice > 98) {
            if (mValue < mMax) {
                set(type, mValue + 0.0001);
            }
        } else if (dice < 2) {
            if (mValue > mMin) {
                set(type, mValue - 0.0001);
            }
        }
    }

private:
    int mIndex;
    double mValue;
    double mMin;
    double mMax;

    DoubleValue();
};
