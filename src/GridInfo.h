#pragma once

#include "Cell.h"

// --------------------
namespace GridInfo {
enum {
    numrows = 200,
    numcols = 200,
    colwidth = 3,
    rowheight = 3,
};
}

extern Cell Cells[GridInfo::numrows][GridInfo::numcols];
