from pyalamake import alamake

# === packages
opengl = alamake.find_package('opengl')

# === C++ tgt
tgt = alamake.create('jxlife', 'cpp')
tgt.add_sources([
    'src/Animal.cpp',
    'src/App.cpp',
    'src/Cell.cpp',
    'src/EnvironmentInfo.cpp',
    'src/Grid.cpp',
    'src/IntParam.cpp',

    'src/main.cpp',
])
tgt.add_include_directories(['src'])
tgt.add_link_libraries('pthread')
tgt.add_include_directories(opengl.include_dir)
tgt.add_link_libraries(opengl.link_libs)

# === gtest
# ut = alamake.create('ut', 'gtest')
# ut.add_include_directories([
#     'src',
# ])
# ut.add_sources([
#     'ut/ut_test1.cpp',
# ])
# ut.add_link_libraries('gtest_main')
# ut.add_coverage(['src'])

# === generate makefile for all targets
alamake.makefile()
