* website: <https://arrizza.com/jxlife.html>
* installation: see <https://arrizza.com/setup-common.html>

## Summary

The original game of Life was described by John Conway here
<https://www.ibiblio.org/lifepatterns/october1970.html>
and a Java implementation is here [Game Of Life](https://www.bitstorm.org/gameoflife/).

This design essentially came from a conversation with my son, Thomas, and I had about evolution and how things interact.

## How to use it

Start it up:

```bash
./doit
```

If all the Animals die, press Re-init!

## Parameters

You can modify these parameters:

* Sun % - the probability the sun shines on a given cell
* Inc Plant w/ Dirt - the plant life increment if there is sunshine and dirt in the cell
* Inc Plant w/out Dirt - the plant life increment if there is sunshine but no dirt in the cell.
* Dec Plant w/out Sun - the plant life decrements if there is no sunshine in the cell
* Sociability - 0 is unsociable, just a voracious eating machine! 100 is "clingy", barely survivable.
* Child Min. Energy - a child is born with at least this much energy
* Parent Energy Threshold - the amount of energy a parent needs to have a child
* Energy to Move - the amount of energy it takes to move closer to food
* Energy Gained Eating - the amount of energy an Animal gets by eating plants
* Energy Loss/Day - the amount of energy an Animal loses per day just by existing
* Plants eaten/day - the amount of plants an Animal eats in a day
* % exists - the percentage of time an Animal is initially created, used to determine its lifetime
* Use Genetics - use genetics to determine the traits of a Child from its parents

## What the colors mean

* The white squares have no sunshine, no plants, no dirt and no Animals.
* The yellow squares have sunshine but no plants, no dirt and no Animals.
* The brown squares might have dirt but, no plants and no Animals, and perhaps some sunshine.
* The green squares:
    * may have plants but no Animals
    * may have dirt
    * may have sunshine.
    * The darker the green, the more plants there are.
* The blue squares:
    * have an Animal
    * may have sunshine, plants and dirt.
    * The darker the blue, the more Energy the Animal has.
* Newborns are "baby blue" :)

## What the text means

* Generation: The number of updates that were done. See below for a description of what happens during a generation.
* Types: Number of Animal Types.
    * If Genetics is off, then usually only 1 type.
    * If you change a parameter, a new Type may show up for a while until the other types die off

* Population: Total number of animals that are alive

* Type: XXXX pop.: the number of cells that have an Animal of Type XXXX.
    * Only the first few are listed.
    * The "XXXX" is an encoding of the Animal's "chromosome".
    * Each letter ("gene") represents a parameter, e.g. how much food the animal uses per day, etc.

## jxLife Initial screen

This screenshot shows jxLife just after 370 generations, a few seconds after start up. The screen is randomly set to
have Animals or not, in this case there are 1,273 Animals. The big die-off has already occurred and six colonies have
started.

![sample](https://arrizza.com/img/jxLife-sample.png){width=600}

This screenshot shows jxLife after a Re-init and at 570 generations. In this run, colonies have merged into two large
clumps.

![after dieoff](https://arrizza.com/img/jxLife-after-dieoff.png){width=600}

## Cool things

After the first initialization is done, there is a big "die-off" where most of the initial Animals die from starvation.
The result is usually less than 100 Animals are on the screen. During the pause where there are few Animals, the plant
life builds up. Then one to four "colonies" start up. Within a short time, the entire screen is filled with Animals
eating at the abundant plant life, usually up to 2000 - 3000.

But eventually the plants run out and another "die-off" starts. The Animals almost go extinct, sometimes down to a few
hundred. The plant life recovers and the Animals re-populate again.

This cycle will continue for hours. It is possible for the population to go extinct, but it's rare.

Try setting Sun% from its default of 32 down to 31, 30, 29 and so on. Notice that the downward swing of the Animal
population goes closer and closer to zero. There will be a point where the Animals do not start up (somewhere around 26
or 27). Reset Sun% to around 32 and click the Re-Init! button to recover.

Try setting the Sun% to 80. It's difficult to describe what seems to happen here. The food is so plentiful, the Animals
go crazy eating and replicating. The color patterns appear kind of "inverted" from the normal case, but that's just
because there are so many Animals.

Now set Sociability to 100 and leave Sunshine% at 80. The Animals clump in tiny tribes of 5 - 10 each. They can split
in two and new tribes show up.

Leave Sociability at 100 and set Sunshine% to 50. The population drops dramatically, most of the time going to zero.

Reset Sun% to 32 and set Sociability to 80. Press Re-init. The Animals form tribes that are "looser" than in the
previous case. Slowly increase Scalability to around 90. There's a bit of die-off, so increase Sun% to 35 or so.
Continue to push Sociability upwards and Sun% upwards while maintaining a low number of animals, say less than 1000.
At some point, you should reach 100 Scalability (it's difficult!) and the Sun% should be around 80 - 85. The tribes
are small, stable, and they don't move very much because they don't have to. The food is there, they can stay close
together without having to chase food because it regrows so quickly.

Now decrease Parent Energy Threshold from 250 to 240. This means it's easier for the Animals to have offspring. The
population increases fairly quickly. Reduce the Sun% to 80 again, and adjust the Parent Energy Threshold downwards to
keep the total population steady.

Notice the total population is in one large clump. Smaller tribes have killed off and the one tribe (actually a close
association of smaller tribes) has formed.

Lower the Child Minimum Energy from 50 to 45. This means that a child is viable with less Energy. The population rises
quickly. Decrease the Sun% to get it back to around 1000.

Now, change Scalability from 100 to 97. The population explodes and the screen is filled within a minute or so.

## What happens every Generation

## Sunshine is calculated

* The presence of sunshine is calculated based on the value in Sun%.

## Plant Life is calculated

* If there is no sunshine
    * the plant life is decremented Plant Life w/out Sun.
    * If that's the end of the plant life, the "dirt" is incremented.
* If there is Sunshine and no Dirt
    * the plant life is incremented Plant Life w/out Dirt.
* If there's Sunshine and Dirt
    * the plant life gets a different increment Plant Life w/ Dirt
    * the dirt is used up i.e. decremented

## Animals use energy

* If there is an Animal in the cell:
    * Its age is incremented
    * It loses a bit of Energy every Generation.
    * Older and very young Animals lose a bit more Energy than the ones in the middle

## Animals die if their energy is too low

* If an Animal's Energy goes to zero or below, the Animal is dead.

## The Circle of Life

* An Animal creates some "dirt" every day.
* If a cell started with an Animal, but it died during the Generation, some dirt is created.
* Dirt feeds the Plant Life

## Animals hunt for food

* An Animal gains Energy by eating plants, if it can.
* It takes some Energy for an Animal to move
* If there isn't enough food in the current location, and it has enough Energy to move:
    * it checks surrounding cells for the highest plant life
    * it moves there

## Animals mate

* If a cell doesn't have an Animal, there is a check if a birth is possible:
    * The immediately surrounding cells are checked for the existence of two Animals.
    * The two with the highest Energy are selected
    * If they both pass a threshold, a baby is born

## It takes energy to have a child

* The Energy of the baby is a minimum plus a percentage from both parents
* Each parent's Energy is decremented by half the amount given to the baby
* If either parent goes below zero, the parent is killed (Energy set to zero).

## Animals can be made sociable

* If the Animal is sociable:
    * The next layer of surrounding cells are checked for the existence of Animals.
    * If the current Animal has the highest Energy, it doesn't move.
    * Otherwise, it moves towards the Animal with the highest Energy.

## Genetics

* If you don't select "Use Genetics":
    * Otherwise, the baby gets the full traits of one parent i.e. it's simple replication
* If you select "Use Genetics":
    * crossover - 50% chance you inherit a trait from one parent or another
    * mutation - 1 in 1000 chance a trait spontaneously increases or decreases.
